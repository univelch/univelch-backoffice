import os

from app import create_app
from app.config import BASE_DIR


app = create_app(os.path.join(BASE_DIR, 'app/config/config.json'))


if __name__ == '__main__':
    app.run(threaded=True)
