from flask import Flask
from flask_cors import CORS


def create_app(json_config):
    app = Flask(__name__)
    CORS(app)
    app.config.from_json(json_config)
    from app.api import bp as api_bp
    app.register_blueprint(api_bp, url_prefix='/api')
    return app
