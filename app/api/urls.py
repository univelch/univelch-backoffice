from . import api
from .resources import (SystemCSVResource, DataSQLResource, SystemJSONResource,
                        SystemCSVLigneResource, SystemJSONLigneResource,SystemGlobResource,DataCSVLigneResource,DataCSVResource,PathCSVResource,DataMapperResource)


api.add_resource(SystemCSVResource, '/system/csv/<file_id>/')
api.add_resource(SystemCSVLigneResource,
                                      '/system/csv/<file_id>/ligne/<ligne_id>/')
api.add_resource(DataCSVLigneResource,
                                      '/data/csv/<file_id>/ligne/<ligne_id>/')
api.add_resource(DataCSVResource,
                                      '/data/csv/<file_id>')
api.add_resource(SystemJSONResource, '/system/json/<file_id>/')
api.add_resource(SystemJSONLigneResource,
                                     '/system/json/<file_id>/ligne/<ligne_id>/')
api.add_resource(DataSQLResource, '/data/sql/<file_id>/')
api.add_resource(SystemGlobResource, '/glob/system/')
api.add_resource(PathCSVResource, '/path/csv/<filename>/')
api.add_resource(DataMapperResource, '/data/mapper/<filename>/')
# save SQL in data
