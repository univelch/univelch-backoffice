import os
import json

from flask import request
from flask_restful import Resource

from app.config import BASE_DIR


class SystemJSONLigneResource(Resource):
    def get(self, file_id, ligne_id):
        with open(os.path.join(BASE_DIR, 'system/' + file_id + '.json')) as f:
            return json.dumps([e for e in json.load(f)][int(ligne_id)])

    def post(self, file_id):
        with open(os.path.join(BASE_DIR, 'system/' + file_id + '.json')) as f:
            return json.dumps([e for e in json.load(f)] + [request.json])

    def put(self, file_id, ligne_id):
        with open(os.path.join(BASE_DIR, 'system/' + file_id + '.json')) as f:
            json_content = [e for e in json.load(f)]
            json_content[int(ligne_id)] = [request.json]
            return json.dumps(json_content)

    def delete(self, file_id, ligne_id):
        with open(os.path.join(BASE_DIR, 'system/' + file_id + '.json')) as f:
            json_content = [e for e in json.load(f)]
            del json_content[int(ligne_id)]
            return json.dumps(json_content)
