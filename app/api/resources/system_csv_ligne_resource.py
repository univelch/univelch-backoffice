import os
import csv
import json
from functools import reduce

from flask import request
from flask_restful import Resource

from app.config import BASE_DIR


class SystemCSVLigneResource(Resource):
    def get(self, file_id, ligne_id):
        """Return ligne at ligne_id position in file_id.csv."""
        headers = ['FIELD' + str(i) for i in range(1, 8)]
        return json.dumps(reduce(lambda d1, d2: {**d1, **d2},
                                  list(map(lambda z_obj: {z_obj[0]: z_obj[1]},
                                           list(zip(headers,
                                                [row for row in csv.reader(open(
                                                  os.path.join(BASE_DIR,
                                                  'system/' + file_id +'.csv')),
                                            delimiter=',')][int(ligne_id)]))))))

    def post(self, file_id):
        """Append ligne at EOF file_id.csv."""
        csv_content = [row for row in csv.reader(open(os.path.join(BASE_DIR,
                                  'system/' + file_id +'.csv')), delimiter=',')]
        csv_content += [[request.json[k] for k in request.json]]
        with open(os.path.join(BASE_DIR, 'system/' + file_id +'.csv'), 'w',
                                                         newline='') as outfile:
            csv.writer(outfile, delimiter=',').writerows(csv_content)
        headers = ['FIELD' + str(i) for i in range(1, 8)]
        return json.dumps([reduce(lambda d1, d2: {**d1, **d2},
                                  list(map(lambda z_obj: {z_obj[0]: z_obj[1]},
                                           list(zip(headers, row)))))
                                                        for row in csv_content])

    def put(self, file_id, ligne_id):
        """Update ligne at ligne_id position in file_id.csv."""
        csv_content = [row for row in csv.reader(open(os.path.join(BASE_DIR,
                                  'system/' + file_id +'.csv')), delimiter=',')]
        csv_content[int(ligne_id)] = [request.json[k] for k in request.json]
        with open(os.path.join(BASE_DIR, 'system/' + file_id +'.csv'), 'w',
                                                         newline='') as outfile:
            csv.writer(outfile, delimiter=',').writerows(csv_content)
        headers = ['FIELD' + str(i) for i in range(1, 8)]
        return json.dumps([reduce(lambda d1, d2: {**d1, **d2},
                                  list(map(lambda z_obj: {z_obj[0]: z_obj[1]},
                                           list(zip(headers, row)))))
                                                        for row in csv_content])
    def delete(self, file_id, ligne_id):
        """Delete ligne at ligne_id position from file_id.csv."""
        csv_content = [row for row in csv.reader(open(os.path.join(BASE_DIR,
                                  'system/' + file_id +'.csv')), delimiter=',')]
        del csv_content[ligne_id]
        with open(os.path.join(BASE_DIR, 'system/' + file_id +'.csv'), 'w',
                                                         newline='') as outfile:
            csv.writer(outfile, delimiter=',').writerows(csv_content)
        headers = ['FIELD' + str(i) for i in range(1, 8)]
        return json.dumps([reduce(lambda d1, d2: {**d1, **d2},
                                  list(map(lambda z_obj: {z_obj[0]: z_obj[1]},
                                           list(zip(headers, row)))))
                                                        for row in csv_content])
