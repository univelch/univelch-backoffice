import json
import csv
from flask import request
from flask_restful import Resource
import os

class PathCSVResource(Resource):
    def get(self, filename):
        print(request)
        csv_content = [row for row in csv.reader(open(
                      os.path.join(request.json['FILEPATH'], filename +'.csv')),
                                                                 delimiter=',')]
        return json.dumps(csv_content)

    def post(self, filename):
        # with open(os.path.join(request.json['FILEPATH'], filename +'.csv'), 'w',
        #                                                  newline='') as outfile:
        #     csv.writer(outfile, delimiter=',').writerows(
        #                                                 request.json['CONTENT'])
        csv_content = [row for row in csv.reader(open(
                      os.path.join(request.json['FILEPATH'], filename +'.csv')),
                                                                 delimiter=',')]
        print('post')
        print(request.json['FILEPATH'])
        print(filename)
        print(csv_content)
        return json.dumps(csv_content)

    def put(self, filename):
        with open(os.path.join(request.json['FILEPATH'], filename +'.csv'), 'w',
                                                         newline='') as outfile:
            csv.writer(outfile, delimiter=',').writerows(
                                                        request.json['CONTENT'])
        print('put')
        print(request.json['FILEPATH'])
        print(request.json['CONTENT'])
        print(filename)
        return 'ok'

    def delete(self, filename):
        os.remove(os.path.join(request.json['FILEPATH'] + filename +'.csv'))