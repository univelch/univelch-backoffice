import os
import csv
import json
import ast
import glob
from pandasql import *

from flask import request
from flask_restful import Resource

from app.config import BASE_DIR


class SystemGlobResource(Resource):
    def get(self):
    	globCsv = glob.glob(os.path.join(BASE_DIR,'system/*.csv'))
    	print(globCsv)
    	temp = []
    	for i,val in enumerate(globCsv):
    		splitedPath = val.split('\\')
    		splitedFileName = splitedPath[-1].split('.')
    		temp.append(splitedFileName[0])
    	return temp