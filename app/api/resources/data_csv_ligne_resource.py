import os
import csv
import json
from functools import reduce

from flask import request
from flask_restful import Resource

from app.config import BASE_DIR

from pandasql import *


class DataCSVLigneResource(Resource):
    def get(self, file_id, ligne_id):
        """Return ligne at ligne_id position in file_id.csv."""
        headers = ['FIELD' + str(i) for i in range(1, 8)]
        return json.dumps(reduce(lambda d1, d2: {**d1, **d2},
                                  list(map(lambda z_obj: {z_obj[0]: z_obj[1]},
                                           list(zip(headers,
                                                [row for row in csv.reader(open(
                                                  os.path.join(BASE_DIR,
                                                  'data/' + file_id +'.csv')),
                                            delimiter=',')][int(ligne_id)]))))))

    def post(self, file_id):
        """Append ligne at EOF file_id.csv."""
        with open(os.path.join(BASE_DIR, 'data/' + file_id +'.csv'), 'w',
                                                         newline='') as outfile:
            csv.writer(outfile, delimiter=',').writerows([e for e in list(
                         map(lambda row: [row[k] for k in row], request.json))])

    def put(self, file_id, ligne_id):
        """Update ligne at ligne_id position in file_id.csv."""
        csv_file = [row for row in csv.reader(open(os.path.join(BASE_DIR,
                                  'data/' + file_id +'.csv')), delimiter=',')]
        

        csv_content = {k: request.json[k] for k in request.json}
        df = pd.read_csv(get_data(os.path.join(BASE_DIR, 'data/'+ file_id +
                                                      '.csv')), parse_dates=[0])  # tester si ça marche sans parse_date=[0]
        print(csv_content)
        headers = list(df)
        for header in headers:
            print(csv_content[header])
        csv_file[int(ligne_id)] = [csv_content[k] for k in csv_content]
        with open(os.path.join(BASE_DIR, 'data/' + file_id +'.csv'), 'w',
                                                         newline='') as outfile:
            csv.writer(outfile, delimiter=',').writerows(csv_file)
        # headers = ['FIELD' + str(i) for i in range(1, 8)]
        return json.dumps([reduce(lambda d1, d2: {**d1, **d2},
                                  list(map(lambda z_obj: {z_obj[0]: z_obj[1]},
                                           list(zip(headers, row)))))
                                                        for row in csv_content])

        return 
    def delete(self, file_id, ligne_id):
        """Delete ligne at ligne_id position from file_id.csv."""
        csv_content = [row for row in csv.reader(open(os.path.join(BASE_DIR,
                                  'data/' + file_id +'.csv')), delimiter=',')]
        del csv_content[int(ligne_id)]
        with open(os.path.join(BASE_DIR, 'data/' + file_id +'.csv'), 'w',
                                                         newline='') as outfile:
            csv.writer(outfile, delimiter=',').writerows(csv_content)
        headers = ['FIELD' + str(i) for i in range(1, 8)]
        return json.dumps([reduce(lambda d1, d2: {**d1, **d2},
                                  list(map(lambda z_obj: {z_obj[0]: z_obj[1]},
                                           list(zip(headers, row)))))
                                                        for row in csv_content])
