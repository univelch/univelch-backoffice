import os
import json

from flask import request
from flask_restful import Resource

from app.config import BASE_DIR


class SystemJSONResource(Resource):
    def get(self, file_id):
        """Return system json with name file_id."""
        with open(os.path.join(BASE_DIR, 'system/' + file_id + '.json')) as f:
            return json.load(f)

    def post(self, file_id):
        """Create system json naming it file_id.json."""
        with open(os.path.join(BASE_DIR, 'system/' + file_id + '.json')
                                                                   ) as outfile:
            json.dump(request.json, outfile)
            return json.load(outfile)
