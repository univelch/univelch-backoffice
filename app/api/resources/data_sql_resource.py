import os
import csv
import json
import ast

from pandasql import *

from flask import request
from flask_restful import Resource

from app.config import BASE_DIR


class DataSQLResource(Resource):
    def get(self, file_id):
        """
        df = pd.read_csv(get_data(os.path.join(BASE_DIR, 'data/'+ file_id +'.csv')), parse_dates=[0])
        return sqldf(request.json['q'] + ' df').to_json()
        """

    def post(self, file_id):
        df = pd.read_csv(get_data(os.path.join(BASE_DIR, 'data/'+ file_id +
                                                      '.csv')), parse_dates=[0])  # tester si ça marche sans parse_date=[0]
        print(df)
        return sqldf(request.json['q'].replace('table', 'df')).to_json(orient='table')
