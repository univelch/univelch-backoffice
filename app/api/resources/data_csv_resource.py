import os
import csv
import json
from functools import reduce

from flask import request
from flask_restful import Resource

from app.config import BASE_DIR

from pandasql import *


class DataCSVResource(Resource):
    def get(self, file_id, ligne_id):
        """Return ligne at ligne_id position in file_id.csv."""

    def post(self, file_id):
        """Append ligne at EOF file_id.csv."""
        csv_content = {k: request.json[k] for k in request.json}
        print(request.json)
        return 'test'

    def put(self, file_id, ligne_id):
        """Update ligne at ligne_id position in file_id.csv."""

    def delete(self, file_id, ligne_id):
        """Delete ligne at ligne_id position from file_id.csv."""
