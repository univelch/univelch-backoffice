import os
import csv

from flask_restful import Resource

from app.config import BASE_DIR


class DataMapperResource(Resource):
    def get(self, filename):
        data_csv_content = [row for row in csv.reader(open(
                            os.path.join(BASE_DIR,'data/' + filename +'.csv')),
                            delimiter=',')]
        headers = data_csv_content[0]
        with open(os.path.join(BASE_DIR, 'system/' + filename +'.csv'), 'w',
                                                         newline='') as outfile:
            system_data = ['inputText' for i in range(len(headers))]
            system_data = [list((e[0], 1, e[1], 12, 'transparent', 'auto', 'explorer')) for e in list(zip(system_data, headers))]
            system_data = [['Slide', 1, filename, 12, 'white', 'auto', 'layoutNav']] + system_data
            csv.writer(outfile, delimiter=',').writerows(system_data)