import os
import csv
import json
from functools import reduce

from flask import request
from flask_restful import Resource

from app.config import BASE_DIR


class SystemCSVResource(Resource):
    def __init__(self, delimiter=',', newline='\n', *args, **kwargs):
        super(SystemCSVResource, self).__init__(*args, **kwargs)

    def get(self, file_id):
        """Return the system csv file with name file_id."""
        headers = ['FIELD' + str(i) for i in range(1, 8)]
        return json.dumps([reduce(lambda d1, d2: {**d1, **d2},
                                  list(map(lambda z_obj: {z_obj[0]: z_obj[1]},
                                           list(zip(headers, row))))
                                 ) for row in csv.reader(open(
                                        os.path.join(BASE_DIR,
                                                'system/' + file_id +'.csv')),
                                                                delimiter=',')])

    def post(self, file_id):
        """Create system csv file naming it file_id.csv."""
        with open(os.path.join(BASE_DIR, 'system/' + file_id +'.csv'), 'w',
                                                         newline='') as outfile:
            csv.writer(outfile, delimiter=',').writerows([e for e in list(
                         map(lambda row: [row[k] for k in row], request.json))])
